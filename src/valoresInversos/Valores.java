package valoresInversos;

import java.util.Scanner;

public class Valores {
	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Informe os n�meros separados por ,");
		String number = sc.next();
		String values[] = Valores.separeteComma(number);
		
		System.out.println(reverseValues(values));
		
		
	}

	private static String[] separeteComma(String number) {
		return number.split(",");
	}

	private static String reverseValues(String[] values) {
		String exit = "";

		for (int x = values.length - 1; x >= 0; x--) {

			if (exit.length() == 0) {
				exit = values[x];
			} else {
				exit += ", " + values[x];
			}
		}
		return exit;
	}


	

}
