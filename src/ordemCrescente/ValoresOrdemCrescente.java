package ordemCrescente;

import java.util.Arrays;
import java.util.Scanner;

public class ValoresOrdemCrescente {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Informe os n�meros separados por ,");
		String numero = sc.next();
		String valores[] = ValoresOrdemCrescente.valoresSeparados(numero);
		
		
		Arrays.sort(valores);
		System.out.println(Arrays.toString(valores));
	}

	private static String[] valoresSeparados(String numero) {
		return numero.split(",");
	}
	
	
}
