package salario;

public class Salario {
	
	private Double qtdHoras;
	private Double valorHora;
	
	public Double salarioBruto() {
		return qtdHoras * valorHora;
	}
	
	public Double valorSindicato() {
		return (salarioBruto() * 1.03) - salarioBruto();
	}
	
	public Double getInss() {
		return (salarioBruto() * 1.10) - salarioBruto();
	}

	public Double getIrValor() {

		if (salarioBruto() <= 900) {
			return 0.;
		} else if (salarioBruto() <= 1500) {
			return (salarioBruto() * 1.05) - salarioBruto();
		} else if (salarioBruto() <= 2500) {
			return (salarioBruto() * 1.10) - salarioBruto();
		} else {
			return (salarioBruto() * 1.20) - salarioBruto();
		}
	}

	public Double getFgts() {
		return (salarioBruto() * 1.11) - salarioBruto();
	}

	public Double getTotalDiscount() {
		return getInss() + getIrValor() + valorSindicato();
	}

	public Double getSalarioLiquido() {
		return salarioBruto() - getTotalDiscount();
	}

	@Override
	public String toString() {
		return "Salary: " + salarioBruto() + "\n" + "Liquid Salary: " + getSalarioLiquido() +  "\n" + "FGTS: " + getFgts() +  "\n" + "Total Discount: " + getTotalDiscount() + "\n" ;
	}

	public Double getqtdHoras() {
		return qtdHoras;
	}

	public void setqtdHoras(Double qtdHoras) {
		this.qtdHoras = qtdHoras;
	}

	public Double getvalorHora() {
		return valorHora;
	}

	public void setvalorHora(Double valorHora) {
		this.valorHora = valorHora;
	}
	
}
